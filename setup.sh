#!/bin/sh
#creates a symbolic link in the users local bin directory. This allows the user to execute the script from anywhere

sudo chmod +x backlight.sh
sudo ln -s ${PWD}/backlight.sh /usr/local/bin/backlight
