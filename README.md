# brightness_bashshell_script
A simple bash script for linux to increase brightness, decrease brightness, set brightnes, current brightnes, nightmode and daymode by running the commands in your terminal.
This script needs `notify-send` in order for notifications to work.

## Intallation
Run the following commands in your terminal for cloning and installation:
```
git clone https://gitlab.com/gorlapraveen/brightness_bashshell_script.git

cd brightness_bashshell_script
./setup.sh
```

# Usage
After installation run: `backlight [command] [options]`
```
[command]
    current    : Shows present brightness level within the max brighness level
    increase   : increases brightness
    decrease   : decreases brightness
    set        : sets brightness to an Integer
    nightmode  : Nightmode
    daymode    : Daymode

[options]      : input integers form 1 to maximum brightness
```

# Uninstallation
Run from the installation directory `./delete.sh` in your terminal of choice.

# Binaries
A Debian(.deb) exclusive package is available to download at [Gitlab rep](https://gitlab.com/gorlapraveen/blacklight-deb-packages).
Install it via your favorite package manager or via `dpkg -i <file>`

# Contributors
Originally created by @gorlapraveen.
Improved by the following people:
- @lpkkjhd
