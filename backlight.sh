#!/bin/bash
#copyright (c) 2016 Author(s): Gorla Praven <gorlapraveen@gmail.com>
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or (at your option) any later version.This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#You should have received a copy of the GNU General Public License  along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Check whether the script was run as root if not it tries to run the same script as sudo
# requires the script to be installed via the setup.sh
if [ "$EUID" -ne 0 ] ; then
	sudo backlight $1 $2
	exit 1
fi

# Automatically fetches the display manufacturer and brightness levels
sysfs="/sys/class/backlight/$(ls /sys/class/backlight)"
max="$(cat ${sysfs}/max_brightness)"
level="$(cat ${sysfs}/brightness)"

step="$(($max/25))"
day="$(echo "scale=0; $max*9/16" | bc -l)"
night="$(echo "scale=0; $max*1/16" | bc -l)"

usage() {
	echo "Information about this device:"
	echo "step size:     $step"
	echo "day value:     $day"
	echo "night value:   $night"
	echo "current value: $level"
	echo "max value:     $max"
	echo " "
	echo "Usage:"
	echo "backlight <command>"
	echo " "
	echo "Possible commands (replace <command> parameter):"
	echo "increase    - increases brightness"
	echo "decrease    - decreases brightness"
	echo "set <value> - sets brightness to an Integer "
	echo "night       - Nightmode"
	echo "day         - Daymode"
	exit
}

set_brightness() {
	level="$1"
	if [ "$level" -lt 1 ] ; then
		level=1
	elif [ "$level" -gt "$max" ] ; then
		level="$max"
	fi
	echo "$level" > $sysfs/brightness
}

case "$1" in
	increase)
		((level+=$step)) ;;
	decrease)
		((level=$level-$step)) ;;
	night)
		sudo -u ${SUDO_USER} notify-send "🌙 Night Mode" "decreased the backlight intensity"
		((level=$night))  ;;
	day)
		sudo -u ${SUDO_USER} notify-send "🌅 Day Mode" "increased the backlight intensity"
		((level=$day))  ;;
	set)
		if [[ ! $2 =~ ^[[:digit:]]+$ ]]; then
			echo "expecting an Integer"
			exit 1
		fi
		((level=$2)) ;;
	*)
		echo "Invalid parameters passed!" && usage ;;
esac

set_brightness $level && echo $level
